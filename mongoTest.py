# -*- coding: utf-8 -*-
# @Time : 2022/3/21 13:45
# @Author : Administrator
# @Email : simple_days@qq.com
# @File : mongoTest.py
# @Project : flaskProject
# ©Copyright : 中国电建集团透平科技有限公司版权所有
from pymongo import MongoClient

client = MongoClient('mongodb://172.2.6.93:30002')

db = client.pcttTest
coll = db.user
result = coll.find_one({'id':53},{'_id':0})
print(result)