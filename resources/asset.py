# -*- coding: utf-8 -*-
# @Time : 2022/3/21 9:23
# @Author : Administrator
# @Email : simple_days@qq.com
# @File : asset.py
# @Project : flaskProject
# ©Copyright : 中国电建集团透平科技有限公司版权所有
from flask_restful import Resource, Api, reqparse, abort
assets = {
    'asset1': {'asset_id': 'asset1'},
    'asset2': {'task': '?????'},
    'asset3': {'task': 'profit!'},
}


def abort_if_asset_doesnt_exist(asset_id):
    if asset_id not in assets:
        abort(404, message="asset {} doesn't exist".format(asset_id))

parser = reqparse.RequestParser()
parser.add_argument('task')


# asset
# shows a single asset item and lets you delete a asset item
class Asset(Resource):
    def get(self, asset_id):
        abort_if_asset_doesnt_exist(asset_id)
        return assets[asset_id]

    def delete(self, asset_id):
        abort_if_asset_doesnt_exist(asset_id)
        del assets[asset_id]
        return '', 204

    def put(self, asset_id):
        args = parser.parse_args()
        task = {'task': args['task']}
        assets[asset_id] = task
        return task, 201


# assetList
# shows a list of all assets, and lets you POST to add new tasks
class AssetList(Resource):
    def get(self):
        return assets

    def post(self):
        args = parser.parse_args()
        asset_id = int(max(assets.keys()).lstrip('asset')) + 1
        asset_id = 'asset%i' % asset_id
        assets[asset_id] = {'task': args['task']}
        return assets[asset_id], 201