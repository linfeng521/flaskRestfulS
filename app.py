from flask import Flask
from flask_restful import Resource, Api, reqparse, abort
from resources.user import UserList,User
from resources.asset import Asset,AssetList

app = Flask(__name__)
api = Api(app)
app.config['JSON_AS_ASCII'] = False

##
## Actually setup the Api resource routing here
##
api.add_resource(UserList, '/user/','/users/','/user')
api.add_resource(AssetList, '/asset/')
api.add_resource(User, '/user/<id>','/users/<id>')
api.add_resource(Asset, '/asset/<asset_id>')


if __name__ == '__main__':
    app.run(debug=True,auto_reload=True)